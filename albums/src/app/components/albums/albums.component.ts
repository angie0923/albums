import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Album} from "../../models/Album";
import {DataService} from "../../services/data.service";


@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {
  // properties
  currentStyle: {} = {};
  cardStyle: {} = {};
  // encapsulation: ViewEncapsulation.None


  albums: Album[] = [];
  showForm: boolean = true;
  album: Album = {
    title: '',
    artist: '',
    songs:  '',
    favorite: '',
    year: 0,
    genre: '',
    units: 0,
    cover: "../assets/img/m.png",
    hide: false
  };

  @ViewChild("albumForm") form: any;

  // inject the data service in the constructor
  // dependency injection

  constructor(private dataService: DataService) { }

  ngOnInit(): void {

    // data commented out for observable and services lesson


    // this.albums = [
    //   {title: "Decemberunderground",
    //     artist: "AFI",
    //     songs:  ["Prelude 12/21, Kill Caustic, Miss Murder, Summer Shudder, The Interview, Love Like Winter, Affliction, The Missing Frame, Kiss and Control, The Killing Lights, 37mm, Endlessly, She Said, On the Arrow "],
    //     favorite: "The Interview",
    //     year: 2006,
    //     genre: "Alternative Rock",
    //     units: 993000,
    //     cover: "../assets/img/decemberunderground.png",
    //     hide: false
    //   },
    //   {title: "Mellon Collie and the Infinite Sadness",
    //     artist: "Smashing Pumpkins",
    //     songs:  ["Mellon Collie and the Infinite Sadness, Tonight Tonight, Jellybelly, Zero, Here Is No Why, Zero, To Forgive, Love, Cupid de Locke, Galapogos, Muzzle, Porcelina of the Vast Oceans, Take Me Down, Thirty-Three, 1979"],
    //     favorite: "To Forgive",
    //     year: 1995,
    //     genre: "Alternative Rock",
    //     units: 10000000,
    //     cover: "../assets/img/sp.png",
    //     hide: false
    //   },
    //   {title: "Stadium Arcadium",
    //     artist: "Red Hot Chili Peppers",
    //     songs:  ["Dani California, Snow, Charlie, Stadium Arcadium, Hump de Bump, She's Only 18, Slow Cheetah, Torture Me, Strip My Mind, Especially in Michigan, Warlocks, C'mon Girl, Wet Sand, Hey, Desecreation Smile, Tell Me Baby, Hard to Concentrate, 21st Century "],
    //     favorite: "Snow",
    //     year: 2006,
    //     genre: "Rock",
    //     units: 157000,
    //     cover: "../assets/img/rhcp.png",
    //     hide: false
    //   },
    //   {title: "Eat Me, Drink Me",
    //     artist: "Marilyn Manson",
    //     songs:  ["If I Was Your Vampire, Putting Holes in Happiness, The Red Carpet Grave, They Say Hell's Not Hot, Just A Car Crash Away, Heart-Shaped Glasses, Evidence, Are You the Rabbit?, Mutilation Is the Most Sincere Form of Flattery, You and Me and the Devil Makes Three, Eat Me, Drink Me,"],
    //     favorite: "The Red Carpet Grave",
    //     year: 2007,
    //     genre: "Gothic Rock",
    //     units: 1000000,
    //     cover: "../assets/img/mm.png",
    //     hide: false
    //   },
    //   {title: "The Moment",
    //     artist: "Framing Hanley",
    //     songs:  ["Home, Built for Sin, Hear Me Now, Slow Dance, All in Your Hands, It's Not What They Said, 23 Days, Count Me In, Alone in This Bed, Wave Goodbye, The Fold, Lillipop"],
    //     favorite: "Hear Me Now",
    //     year: 2005,
    //     genre: "Alternative Rock",
    //     units: 1500000,
    //     cover: "../assets/img/fh2.png",
    //     hide: false
    //   },
    //
    //
    // ] // end of array

    this.showForm = false;
    this.setCurrentStyle();
    this.setCardStyle();

    // access the getAlbums() method that's inside our DataService

    this.dataService.getAlbums().subscribe(data => {
      console.log("Getting albums from Data Service")
      this.albums = data;
    });



  } // end of ngOnInIt

  toggleInfo(albums: any){
    albums.hide = !albums.hide
  }

  setCurrentStyle(){
   this.currentStyle = {
     "background-color" : "black"
   }
  }

  setCardStyle(){
    this.cardStyle = {
      "background-color" : "gray"
    }
  }

  toggleForm(){
    this.showForm = !this.showForm;
  }

  addAlbum(){
    this.albums.unshift(this.album);
    this.album = {
    title: '',
      artist: '',
      songs:  '',
      favorite: '',
      year: 0,
      genre: '',
      units: 0,
      cover: "../assets/img/m.png",
      hide: false

    }
  }

  // submit
  onSubmit({value, valid} : {value: Album, valid: boolean}){
    if (!valid){
      alert("Form is not valid")
    }
    else {
      value.hide = false
      this.albums.unshift(value);
      this.form.reset();
    }
  }


}
