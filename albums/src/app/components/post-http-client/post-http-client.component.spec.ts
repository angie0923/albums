import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostHttpClientComponent } from './post-http-client.component';

describe('PostHttpClientComponent', () => {
  let component: PostHttpClientComponent;
  let fixture: ComponentFixture<PostHttpClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostHttpClientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostHttpClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
