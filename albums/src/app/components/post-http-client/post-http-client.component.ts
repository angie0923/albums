import { Component, OnInit } from '@angular/core';
import {PostHttpClient} from "../../models/PostHttpClient";
import {PostService} from "../../services/post.service";

@Component({
  selector: 'app-post-http-client',
  templateUrl: './post-http-client.component.html',
  styleUrls: ['./post-http-client.component.css']
})
export class PostHttpClientComponent implements OnInit {

  // properties

  posts: PostHttpClient[] = [];

  // inject our service as a dependency

  constructor(private postsService: PostService) { }


  //fetch the posts when ngOnInIt() method is initialized --

  ngOnInit(): void {

    // subscribe to Observable method that's in our PostsService

    this.postsService.getPosts().subscribe(data => {
      console.log(data);

      // reassigning our property 'posts' to equal data
      this.posts = data;
    })


  }

}
