export interface Album {
  title: string,
  artist: string,
  songs: any,
  favorite: string,
  year: number,
  genre: string,
  units: number,
  cover?: string,
  hide: boolean
}
