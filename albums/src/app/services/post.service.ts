import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {PostHttpClient} from "../models/PostHttpClient";

@Injectable({
  providedIn: 'root'
})
export class PostService {
  // properties
  // set a url as a property

  postsUrl: string = "https://jsonplaceholder.typicode.com/posts";

  // inject the HttpClientModule as a dependency
  constructor(private httpClient: HttpClient) { }

  // create a method that will make our GET reques to the postsUrl
  // use observable method

  getPosts(): Observable<PostHttpClient[]> {

    // returning all the data that comes with the postUrl property
    return this.httpClient.get<PostHttpClient[]>(this.postsUrl)
  }



}
