import { Injectable } from '@angular/core';
import {Album} from "../models/Album";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  // properties

  // initialize the albums property and assign it to an array
  // of the Album interface

  albums: Album[] = [];

  constructor() {

    this.albums = [

      {

        title: "Decemberunderground",
        artist: "AFI",
        songs:  ["Prelude 12/21, Kill Caustic, Miss Murder, Summer Shudder, The Interview, Love Like Winter, Affliction, The Missing Frame, Kiss and Control, The Killing Lights, 37mm, Endlessly, She Said, On the Arrow "],
        favorite: "The Interview",
        year: 2006,
        genre: "Alternative Rock",
        units: 993000,
        cover: "../assets/img/decemberunderground.png",
        hide: false
      },
      {title: "Mellon Collie and the Infinite Sadness",
        artist: "Smashing Pumpkins",
        songs:  ["Mellon Collie and the Infinite Sadness, Tonight Tonight, Jellybelly, Zero, Here Is No Why, Zero, To Forgive, Love, Cupid de Locke, Galapogos, Muzzle, Porcelina of the Vast Oceans, Take Me Down, Thirty-Three, 1979"],
        favorite: "To Forgive",
        year: 1995,
        genre: "Alternative Rock",
        units: 10000000,
        cover: "../assets/img/sp.png",
        hide: false
      },
      {title: "Stadium Arcadium",
        artist: "Red Hot Chili Peppers",
        songs:  ["Dani California, Snow, Charlie, Stadium Arcadium, Hump de Bump, She's Only 18, Slow Cheetah, Torture Me, Strip My Mind, Especially in Michigan, Warlocks, C'mon Girl, Wet Sand, Hey, Desecreation Smile, Tell Me Baby, Hard to Concentrate, 21st Century "],
        favorite: "Snow",
        year: 2006,
        genre: "Rock",
        units: 157000,
        cover: "../assets/img/rhcp.png",
        hide: false
      },
      {title: "Eat Me, Drink Me",
        artist: "Marilyn Manson",
        songs:  ["If I Was Your Vampire, Putting Holes in Happiness, The Red Carpet Grave, They Say Hell's Not Hot, Just A Car Crash Away, Heart-Shaped Glasses, Evidence, Are You the Rabbit?, Mutilation Is the Most Sincere Form of Flattery, You and Me and the Devil Makes Three, Eat Me, Drink Me,"],
        favorite: "The Red Carpet Grave",
        year: 2007,
        genre: "Gothic Rock",
        units: 1000000,
        cover: "../assets/img/mm.png",
        hide: false
      },
      {title: "The Moment",
        artist: "Framing Hanley",
        songs:  ["Home, Built for Sin, Hear Me Now, Slow Dance, All in Your Hands, It's Not What They Said, 23 Days, Count Me In, Alone in This Bed, Wave Goodbye, The Fold, Lillipop"],
        favorite: "Hear Me Now",
        year: 2005,
        genre: "Alternative Rock",
        units: 1500000,
        cover: "../assets/img/fh2.png",
        hide: false

      },


    ] // end of array

  } // end of constructor

  // METHODS GO HERE:

  // create a method that will return an array of this.albums using
  // observable

  getAlbums() : Observable<Album[]> {
    return of(this.albums)
  }






}
