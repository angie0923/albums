import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from "@angular/common/http";
import { AppComponent } from './app.component';
import { AlbumsComponent } from './components/albums/albums.component';
import {FormsModule} from "@angular/forms";
import { PostHttpClientComponent } from './components/post-http-client/post-http-client.component';

@NgModule({
  declarations: [
    AppComponent,
    AlbumsComponent,
    PostHttpClientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
